---

-   name: Install build essentials
    apt: name=build-essential state=latest

-   name: Install debian dev stuff
    apt: name={{ item }} state=latest
    with_items:
        - dpkg-dev
        - devscripts
        - debootstrap
        - dupload
        - fakeroot
        - debhelper
        - dh-make
        - dh-make-perl
        - dh-buildinfo
        - cdbs
        - dpatch
        - kernel-package
        - lintian
        - alien
        - po-debconf
        - pbuilder
        - dpkg-repack

-   name: Install build helpers
    apt: name={{ item }} state=latest
    with_items:
        - make
        - make-doc
        - autoconf
        - autoconf-doc
        - automake
        - cmake
        - ccache
        - ant
        - ant-optional
        - scons
        - qt5-qmake
        - qt4-qmake
        - autogen
        - autotools-dev
        - autoproject
        - kbuild
        - premake4
        - maven

-   name: Install OpenJDK
    apt: name={{ item }} state=latest
    with_items:
        - openjdk-8-jdk

-   name: Install Debuggers
    apt: name={{ item }} state=latest
    with_items:
        - strace
        - gdb
        - elfutils
        - valgrind
        - libc6-dbg

-   name: Install interpreters
    apt: name={{ item }} state=latest
    with_items:
        - python3
        - python
        - perl
        - ruby

-   name: Install dev docs
    apt: name={{ item }} state=latest
    with_items:
        - manpages-dev
        - gcc-doc
        - cpp-doc
        - binutils-doc
        - manpages-posix-dev
        - glibc-doc
        - glibc-doc-reference
        - gdb-doc
        - stl-manual
        - perl-doc

-   name: Install x86 assembly tools
    apt: name={{ item }} state=latest
    with_items:
        - bin86
        - nasm
        - yasm

-   name: Install scanning and parsing utils
    apt: name={{ item }} state=latest
    with_items:
        - flex
        - flex-doc
        - bison
        - bison-doc
        - ragel
        - byacc
        - re2c

-   name: Install VCS
    apt: name={{ item }} state=latest
    with_items:
        - git
        - git-email
        - git-cvs
        - git-svn
        - git-arch
        - git-doc
        - git-buildpackage
        - git-lfs
        - mercurial
        - subversion
        - subversion-tools
        - darcs
        - bzr
        - bzrtools
        - cvs
        - cvsps
        - wdiff
        - colordiff
        - patch
        - patchutils
        - quilt
        - gitk
        - git-gui
        - diffutils

-   name: Install VCS (Ubuntu)
    apt: name={{ item }} state=latest
    with_items:
        - tkdiff
    when: ansible_distribution == "Ubuntu"

-   name: Install mono
    apt: name={{ item }} state=latest
    with_items:
        - mono-complete
        - mono-devel
        - mono-profiler
        - mono-tools-devel
        - mono-tools-gui
        - mono-vbnc

-   name: Install haskell
    apt: name={{ item }} state=latest
    with_items:
        - ghc
        - ghc-doc
        - haskell-platform

-   name: Install haskell on ubuntu
    apt: name={{ item }} state=latest
    with_items:
        - haskell-stack
    when: ansible_distribution == "Ubuntu"

-   name: Install ocaml
    apt: name={{ item }} state=latest
    with_items:
        - ocaml
        - camlp4

-   name: Install python and libs
    apt: name={{ item }} state=latest
    with_items:
        - python3-virtualenv
        - python3-pip
        - python3-all-dev
        - python-pip
        - python-virtualenv
        - drpython
        - ipython
        - pylint
        - pychecker
        - pyflakes
        - python-all-dev
        - python-m2crypto
        - python-mysqldb
        - python-imaging
        - python-libxml2
        - python-libxslt1
        - python-numpy
        - python-numpy-doc
        - python-twisted
        - python-openssl
        - python-pexpect
        - python-pyasn1
        - python-pysnmp4
        - python-sqlobject
        - python-subversion
        - python-urwid
        - python-xmpp
        - python-beautifulsoup
        - python-chardet
        - python-kerberos
        - python-mako
        - python-pyme
        - python-rpm
        - python-selinux
        - python-setuptools
        - python-simplejson
        - python-sqlite
        - python-sqlitecachec
        - python-urlgrabber
        - python-cups
        - python-notify
        - python-tk
        - jython
        - jython-doc

-   name: Install ruby
    apt: name={{ item }} state=latest
    with_items:
        - ruby-dev
        - rake

-   name: Install llvm
    apt: name={{ item }} state=latest
    with_items:
        - clang
        - llvm
        - llvm-dev

-   name: Install scheme/racket
    apt: name={{ item }} state=latest
    with_items:
        - racket
        - racket-doc
        - scheme2c
        - guile-1.8
        - guile-1.8-dev
        - tinyscheme
        - chicken-bin
        - mit-scheme-doc
        - scheme9
        - stalin

-   name: Install tcl/tk
    apt: name={{ item }} state=latest
    with_items:
        - tcl
        - tcl-dev
        - tcl8.5
        - tk
        - tk-dev

-   name: Install lua
    apt: name={{ item }} state=latest
    with_items:
        - lua5.3
        - liblua5.3-dev
        - love

-   name: Install misc scripting
    apt: name={{ item }} state=latest
    with_items:
        - swig
        - gawk
        - mawk
        - m4

-   name: Install misc languages
    apt: name={{ item }} state=latest
    with_items:
        - gfortran
        - erlang
        - erlang-dev
        - beef
        - intercal
        - rhino
        - sbcl
        - golang
        - r-base-core

-   name: Install misc languages on Ubuntu
    apt: name={{ item }} state=latest
    with_items:
        - php-cli
    when: ansible_distribution == 'Ubuntu'

-   name: Install misc languages on Debian
    apt: name={{ item }} state=latest
    with_items:
        - php5-cli
    when: ansible_distribution == 'Debian'

-   name: Install AVR tools
    apt: name={{ item }} state=latest
    with_items:
        - gcc-avr
        - binutils-avr
        - gdb-avr
        - avrdude
        - avrdude-doc
        - simulavr
        - avarice
        - avr-libc
        - avra
        - avrp
        - uisp
        - usbprog

-   name: Install misc libs
    apt: name={{ item }} state=latest
    with_items:
        - libldap2-dev
        - libpam0g-dev
        - comerr-dev
        - e2fslibs-dev
        - libcap-dev
        - libattr1-dev
        - libacl1-dev
        - libselinux1-dev
        - libpcre3-dev
        - libapt-pkg-dev
        - linux-libc-dev
        - libkrb5-dev
        - libreadline-dev
        - libncurses5-dev
        - libncursesw5-dev
        - libghc-mtl-dev
        - libghc-mtl-prof
        - libghc-mtl-doc
        - libghc-network-dev
        - libghc-parsec3-dev
        - libboost-dev
        - zlib1g-dev
        - libbz2-dev
        - libcurl4-gnutls-dev
        - libexpat1-dev
        - libglib2.0-dev
        - libgmp3-dev
        - libsasl2-dev
        - libsctp-dev
        - libsqlite3-dev
        - libssl-dev
        - libxml2-dev
        - libxslt1-dev
        - ss-dev
        - mesa-common-dev
        - uuid-dev
        - libprotobuf-c-dev
        - libprotobuf-dev
        - libffi-dev
        - libidl-dev
        - libelf-dev
        - libdw-dev
        - libaudit-dev
        - libgmp-dev
        - libmpfr-dev
        - libmpc-dev
        - libfplll-dev

-   name: Install misc libs on Ubuntu
    apt: name={{ item }} state=latest
    with_items:
        - libgnutls-dev
    when: ansible_distribution == 'Ubuntu'

-   name: Install x11 libs
    apt: name={{ item }} state=latest
    with_items:
        - xorg-dev
        - libgtk2.0-dev
        - libqt4-dev
        - libsdl1.2-dev
        - freeglut3-dev
        - libgl1-mesa-dev
        - libglu1-mesa-dev
        - libghc-x11-dev
        - libghc-x11-doc
        - libghc-x11-prof
        - libgtkmm-2.4-dev
        - glade
        - xutils-dev

-   name: Install IDEs
    apt: name={{ item }} state=latest
    with_items:
        - eclipse
        - monodevelop

-   name: Install cross toolchains
    apt: name={{ item }} state=latest
    with_items:
        - gcc-5-arm-linux-gnueabi
        - gcc-5-mips-linux-gnu
        - gcc-5-mipsel-linux-gnu
        - gcc-5-powerpc-linux-gnu
        - gcc-5-sparc64-linux-gnu
        - gcc-5-s390x-linux-gnu
    when: ansible_distribution == 'Ubuntu'

-   name: Install misc dev tools
    apt: name={{ item }} state=latest
    with_items:
        - gperf
        - gettext
        - check
        - dejagnu
        - doxygen
        - exuberant-ctags
        - indent
        - cutils
        - chrpath
        - openmpi-bin
        - openmpi-doc
        - protobuf-compiler
        - protobuf-c-compiler

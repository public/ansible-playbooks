#!/bin/bash

if zenity --question --text "Are you sure you want to reset your xsession config file? (will be backed up in ~/.xsession-backup)"; then
  mv .xsession .xsession-backup
  cp /users/skel/.xsession .
fi

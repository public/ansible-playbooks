---

-   name: Install Editors
    apt: name={{ item }} state=latest
    with_items:
        - nvi
        - bvi
        - wily
        - vim-nox


-   name: Install Crypto tools (including smartcard support)
    apt: name={{ item }} state=latest
    with_items:
        - gnupg-agent
        - gnupg2
        - openssl
        - pcscd
        - opensc
        - gpgsm
        - keychain


-   name: Install network tools
    apt: name={{ item }} state=latest
    with_items:
        - ipset
        - ipcalc
        - iftop
        - nmap
        - openipmi
        - ipmitool
        - cdpr
        - lksctp-tools
        - iptables
        - ebtables
        - arptables
        - freeipmi-tools
        - lftp
        - ncftp
        - axel
        - tsocks
        - socat
        - snmp
        - snmp-mibs-downloader
        - finger

-   name: Install http tools
    apt: name={{ item }} state=latest
    with_items:
        - w3m
        - links
        - elinks
        - lynx
        - curl
        - wget

-   name: Install typesetting tools (TeX)
    apt: name={{ item }} state=latest
    with_items:
        - groff
        - texlive-full
        - dvi2ps
        - dvipng
        - dvidvi
        - djvulibre-bin
        - ghostscript
        - psutils

-   name: Install fs tools
    apt: name={{ item }} state=latest
    with_items:
        - fuse
        - sshfs
        - dosfstools
        - davfs2
        - cloop-utils
        - ecryptfs-utils


-   name: Install database tools
    apt: name={{ item }} state=latest
    with_items:
        - sqlite
        - sqlite3
        - sqlite3-doc
        - postgresql-client
        - mysql-client
        - db-util

-   name: Install math tools
    apt: name={{ item }} state=latest
    with_items:
        - graphviz
        - r-recommended
        - euler
        - euler-doc
        - gnuplot-doc
        - gap
        - lp-solve
        - mathomatic
        - maxima
        - maxima-doc
        - fplll-tools
        - gnuplot-nox
        - coq


-   name: Install xml tools
    apt: name={{ item }} state=latest
    with_items:
        - xsltproc
        - libxml2-utils
        - docbook-xsl
        - docbook-utils
        - docbook-dsssl
        - docbook-xml
        - docbook-to-man
        - xmlto
        - fop
        - opensp
        - sgml2x
        - sgmltools-lite
        - sp
        - tidy
        - xalan

-   name: Install multimedia tools
    apt: name={{ item }} state=latest
    with_items:
        - festival
        - dvgrab
        - cdparanoia
        - genisoimage
        - sox
        - mp3blaster
        - x264
        - mkvtoolnix
        - vorbis-tools
        - timidity
        - dvd+rw-tools
        - lame
        - twolame
        - cdrdao
        - wodim
        - pulseaudio
        - bchunk

-   name: Install multimedia tools on Ubuntu
    apt: name={{ item }} state=latest
    with_items:
        - mencoder
        - dvdrip
        - ffmpeg
    when: ansible_distribution == 'Ubuntu'

-   name: Install multimedia tools on Debian
    apt: name={{ item }} state=latest
    with_items:
        - libav-tools
    when: ansible_distribution == 'Debian'


-   name: Install torrent tools
    apt: name={{ item }} state=latest
    with_items:
        - rtorrent

-   name: Install communications tools
    apt: name={{ item }} state=latest
    with_items:
        - mutt
        - alpine
        - tin
        - irssi
        - fetchmail
        - slrn
        - spamassassin
        - weechat
        - weechat-plugins
        - newsbeuter

-   name: Install serial tools
    apt: name={{ item }} state=latest
    with_items:
        - minicom
        - cu

-   name: Install virtualization tools
    apt: name={{ item }} state=latest
    with_items:
        - qemu
        - qemu-kvm
        - libvirt-bin
        - bridge-utils

-   name: Install misc tools
    apt: name={{ item }} state=latest
    with_items:
        - rdiff
        - tofrodos
        - mc
        - busybox-static
        - most
        - bonnie++
        - time
        - latencytop
        - bsdmainutils
        - dctrl-tools
        - gddrescue
        - file
        - parchive
        - par2
        - reportbug
        - rpm
        - shtool
        - smbclient
        - stow
        - xstow
        - sysstat
        - unison
        - john
        - sg3-utils
        - dstat
        - rstat-client
        - i2c-tools
        - dnstracer
        - apt-file
        - makepasswd
        - syslinux
        - fdupes
        - cowsay
        - 9base
        - libjpeg-progs
        - jq
